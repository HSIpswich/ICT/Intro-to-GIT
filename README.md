# Introduction To GIT

### Materials Compiled and made by HS Ipswich (members) for Workshop / Course.


This course material covers an Introduction to GIT ( version control ) for beginners to manage files, code, and documentation.

This course teachers how to use GIT to add version control to documents, guides, software, and firmware for a wide range of projects.

Commonly many open source projects use git to manage their code and documentation. 

Many companies are now taking advantage of the ability to control the versions of their documents, and code that GIT has become a defacto standard to managing these resources.

Wanting to learn more these resources may help.

### Note's:

Slides within this repository are written in markdown specific for making remark slides useing the remarkjs styling.

### Licence Information

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
