class: middle, center
<!-- Insert Logo here -->

![](image\HS_Logo_WIDE_GoogleLogo.png)

# Introduction to Git

### Presenter: Robert Manietta<!-- Change name as appropriate -->

### Cause Leader IT&T <!-- Change title as appropriate -->

---
class: middle
## What is GIT?

- git is version control system applied against text based documents.

- Created by Linus Torvalds [The creator or the linux kernel]

>To better manage the linux kernel development as it exploded and took off previously the kernel was managed via email and a few other version control systems but none really meet with the high standards Linus was holding them too.

---
class: middle

## Installing GIT

- To intall git you need to haed over to

- [http://git-scm.com](http://git-scm.com)

- Download and install the version appropriate to your operating system / it's Architecture (64 bit/32 bit).

---
class: middle
## Global Configuration

- git config --global user.name 'firstname lastname'

- git config --global user.email 'user@eg.com'

- git config --global core.editor '/bin/vim'  windows eg. C:/progra~1/notepad.exe

- .gitignore - what to ignore in a repository not to be tracked.


---
class: middle
## Creating a local Repository

  - Create a folder for the repository

  - git init - this will initialize the directory for tracking by git weather the folder is empty or not

  - That's it we now have a local repository that is initialized and ready to track any documents we create within it.

---
class: middle

## Adding files

  - Create a file in the folder call it " test.txt "

  - run git status it tells us what is tracked what has changed and what is yet to be committed to our repository since the last commit if any.

  - To add the file to our staging area ready for committing

  - git add test.txt or git add . ( . will add all files )

  - git status - will now show test.txt in staging ready for commit.

  - git commit -m 'message' if a message isn't added inline like this the default editor will open giving the user the opportunity to add the message manually.

  - git status now should show nothing outstanding to commit, and no changed files out standing to add.

---
class: middle

## Removing tracked files

- Removing a tracked file,

- git rm "filename"

- This removes the file from the directory, as well as from the repository.

- If you just delete the file from the directory on the next commit it will error as it's missing the file it believes should be there.

- To fix this just run the git rm "filename" then retry the commit this will update the tracking repository to show the file should no longer be tracked.

- Later on we will cover how to do something similar to this to not track files of a certain type. NOTE if they are not tracked they will not be added to remote repositories.

---
class: middle
## Adding Remote Servers & Cloning

  - git remote add [remote server address] (The server we will use today is at 10.1.10.1 )

  - git clone "remote repository"

    - will pull a copy of the master for the remote repository supplied

    - This can then be made into a branch for a working directory

#### _NOTE: Work should always be done in a working directory & branch not within the local master branch._

---
class: middle
## Branching

- git branch - shows list of all branches and indicates the current active branch
- git checkout [branch] - Changes the working/active branch
- git checkout -b [branch] - creates and changes to this new branch

---
class: middle
## Merging and Pushing Repositories

- From the branch you want to _merge into_
- git merge [branch from] - merges into the current branch from the branch named
- git push origin [branch] - pushes the branch name entered to the origin = remote server

---
class: middle, center
# Assignment Time
<hr />
## On completion please present to instructor for review.

<!-- Insert Logo here optionally ...  -->
<hr />
### Then pack up your work space neatly.

---
class: middle
## What now?

#### HSIpswich welcomes you to practice your new skills at our Public Open times, as these vary please check our website.
http://www.hsipswich.org.au
##### Once your participation certificate is presented you are free to leave.
<hr>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
