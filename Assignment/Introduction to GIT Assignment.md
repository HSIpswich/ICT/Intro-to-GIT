## GIT Assignment

You have been supplied a mini server ( raspberry pi) already configured as a git server to use as your remote server accessible via dns when connected via ethernet cable (remote.git.server), with the username "git" and the password "P@ssw0rd1234". [ Note the password is case sensitive.]

Set up git's global configuration settings, Create a new empty repository called MyRepo.

Add a file called README.md containing some text of your choice in the the MyRepo repository commit this file to the local repository using a meaningful message.

Using the provided server push your local MyRepo repository to the remote git server.

Create a local branch called "DEV", in this branch create a file called dev.txt containing the text " Hi from the DEV branch." Commit these changes to the local repository then push them to the remote server.

Create another local branch called "QA", in this branch create the file qaonly.txt containing the text. " This is from the QA Branch." Commit these changes to the local and remote servers.

Merge QA branch into the Master branch and push once again to the remote server.

Have an Instructor / Learning assistant check the work on both the local and remote repositories.

<!-- Instuctors should use git log --graph --decorate to see the students solution. -->

Once instructed to delete the files in the local repository and then the repository folder itself, power off the devices provided and pack them neatly into the collection box provided.

Once all participants have completed the assignment, we will close up the training in the mean time please do not help others complete the assignment.

You are more than welcome to enjoya break with tea, coffee & biscuits while you wait.

<hr>

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
