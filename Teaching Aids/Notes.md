# GIT Course Notes

Not all organizations have access to a Git server or GitLab Instance locally, that being said where possible one should be set up exclusively for use during this training.

Where not possible students should be guided through the process of setting up an account on either Github, bitbucket, or gitlab.

It should be noted that Gitlab will give the students more exposure to added features like bug trackers, private and public repositories.

Github will limit students to public repositories only.

This is just some information to assist instructors select and use the git instance of their choice when teaching students about git.

If providing students with an overview of SSH it's best to make sure students understand best safety practices ie using key pairs. Rather than passwords to authenticate..

GitLab requires SSH Key pairs for pushing to GitLab.

Slides were made in marp and will be exported to PDF, along with Cheat sheets and other class materials.
