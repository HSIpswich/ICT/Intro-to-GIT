## HSIpswich  ~ Git Cheat Sheet


### Global Configuration Commands

- git config --global user.name "name" - configures the default repository username
- git config --global user.email "email" - configures the default repository user email
- git config --system core.editor "full-path-to/editor" - Sets the default editor for commits and diffs
- git config --global alias."aliasname" 'git commands' - Allows you to create and alias for common git Commands

### Basic Commands

- git init "dir name" - Creates an empty repository in the directory dir name and initializes that repository
- git clone "repo" - clones a remote or local repository to the current directory
- git add "files/dir" - Adds the indicated files/directories to the staging area ready for commit to the repository
- git commit -m "message" - commits all the staged files (new/changes) to the current repository in the active branch
- git status - displays the status fo the current repository/branch files and directories (untracked/staged tracked)
- git rm "file" - removes a file from the existing repository cache, staging it for removal during next commit. Will also remove physical file from filesystem.

### Logging Commands

- git log - Displays all commits to the current repository, all branches, newest to oldest
- git log -p -  Displays all commits, with the backend statistics indicating all changes/adds/deletes
- git log --oneline - Displays all commits by short ID and commit message
- git log --stat - Displays all commits, including which files are changed and the number of lines added/deleted from each.
- git log --author="name" - Displays all commits by indicated author
- git lob --grep="value" - Displays all commits with text values matching the passed value.
- git log --graph --decorate - Draws a text based graph of the commits and commit messages with names of branches / tags.


### Branchin and Merging Commands

- git branch - Indicates the current branches and the active branch
- git branch "name" - Creates a new branch by the name provided
- git checkout " branch name" - Changes the active branch to the specified branch
- git checkout -b " branch name" - Creates a new branch and changes it to the active branch
- git merge "name" - Merges all changes/additions/deletions from the indicated branch into the current active branch.

### Remote Server (Public/Private)

- git remote add "server address" - Sets new remote 'origin'
- git remote -v - verifies the new remote server

### Update / Push

- git push "remote" "branch" - Pushes commited changes from local repository current branch to the remote repository same branch 'If branch doesn't exisit in the remote it will be created'
- git push "branch" - Forces a push to the remote repository even if it would result in merge errors
- git push "remote" --all  - Pushes all changes from all local branches to the remote repository creating any branches as necessary.

<hr />

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
